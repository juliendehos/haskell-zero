module Tictactoe.GameSpec (main, spec) where

import Data.Array
import Test.Hspec

import Tictactoe.Game

main :: IO ()
main = hspec spec

runMoves :: [(Int, Int)] -> Game
runMoves = foldl (\g (i,j) -> play i j g) newGame

spec :: Spec
spec = do

    describe "newGame" $ do
        let g = newGame
        it "board" $ _board g `shouldBe`
            (listArray ((0,0),(2,2))
                [ Nothing, Nothing, Nothing
                , Nothing, Nothing, Nothing
                , Nothing, Nothing, Nothing
                ])
        it "player" $ _player g `shouldBe` (Just PlayerX)
        it "running" $ _running g `shouldBe` True
        it "nmoves" $ _nmoves g `shouldBe` 0

    describe "11 -> running" $ do
        let g = play 1 1 newGame
        it "board" $ _board g `shouldBe`
            (listArray ((0,0),(2,2))
                [ Nothing,      Nothing, Nothing
                , Nothing, Just PlayerX, Nothing
                , Nothing,      Nothing, Nothing
                ])
        it "player" $ _player g `shouldBe` (Just PlayerO)
        it "running" $ _running g `shouldBe` True
        it "nmoves" $ _nmoves g `shouldBe` 1

    describe "11 00 02 10 20 -> X wins" $ do
        let g = runMoves [(1,1), (0,0), (0,2), (1,0), (2,0)]
        it "board" $ _board g `shouldBe`
            (listArray ((0,0),(2,2))
                [ Just PlayerO,      Nothing, Just PlayerX
                , Just PlayerO, Just PlayerX,      Nothing
                , Just PlayerX,      Nothing,      Nothing
                ])
        it "player" $ _player g `shouldBe` (Just PlayerX)
        it "running" $ _running g `shouldBe` False
        it "nmoves" $ _nmoves g `shouldBe` 5

    describe "00 20 10 21 01 22 -> O wins" $ do
        let g = runMoves [(0,0), (2,0), (1,0), (2,1), (0,1), (2,2)]
        it "board" $ _board g `shouldBe`
            (listArray ((0,0),(2,2))
                [ Just PlayerX, Just PlayerX,      Nothing
                , Just PlayerX,      Nothing,      Nothing
                , Just PlayerO, Just PlayerO, Just PlayerO
                ])
        it "player" $ _player g `shouldBe` (Just PlayerO)
        it "running" $ _running g `shouldBe` False
        it "nmoves" $ _nmoves g `shouldBe` 6

    describe "11 02 22 00 01 21 10 12 20 -> tie" $ do
        let g = runMoves [(1,1), (0,2), (2,2), (0,0), (0,1), (2,1), (1,0), (1,2), (2,0)]
        it "board" $ _board g `shouldBe`
            (listArray ((0,0),(2,2))
                [ Just PlayerO, Just PlayerX, Just PlayerO
                , Just PlayerX, Just PlayerX, Just PlayerO
                , Just PlayerX, Just PlayerO, Just PlayerX
                ])
        it "player" $ _player g `shouldBe` Nothing
        it "running" $ _running g `shouldBe` False
        it "nmoves" $ _nmoves g `shouldBe` 9

