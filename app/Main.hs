
import Control.Monad (when)
import Text.Read (readMaybe)

import Tictactoe.Game
import Tictactoe.View

run :: Game -> IO ()
run g = do
    printGame g
    when (_running g) $ do
        putStrLn "\ni j ?"
        ijM <- map readMaybe . words <$> getLine
        case ijM of
            [Just i, Just j] -> run (play i j g)
            _ -> do
                putStrLn "bad input"
                run g
        
main :: IO ()
main = run newGame

