module Bot.Bot where

import System.Random

import Tictactoe.Game

class Bot b where
    newBot :: StdGen -> b
    chooseAction :: Game -> b -> (Int, b)

