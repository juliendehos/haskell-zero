module Bot.BotRandom where

import System.Random

import Bot.Bot
import Tictactoe.Game

newtype BotRandom = BotRandom { _gen :: StdGen }

instance Bot BotRandom where

    newBot = BotRandom

    chooseAction g b = BotRandom <$> randomR (0, 9) (_gen b)
    -- chooseAction g b = BotRandom <$> randomR (0, getNbLegalActions g - 1) (_gen b)
    -- TODO

