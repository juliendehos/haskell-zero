module Tictactoe.Game 
    ( Game(..)
    , Player(..)
    , newGame
    , play
    ) where

import Data.Array

data Player
    = PlayerX
    | PlayerO
    deriving (Eq, Show)

type Ix2 = (Int, Int)
type Board = Array Ix2 (Maybe Player)

data Game = Game
    { _board   :: Board
    , _player  :: Maybe Player
    , _running :: Bool
    , _nmoves  :: Int
    }

newGame :: Game
newGame = Game
    (listArray ((0,0),(2,2)) (replicate 9 Nothing))
    (Just PlayerX)
    True
    0

play :: Int -> Int -> Game -> Game
play i j g =
    if i>=0 && i<=2 && j>=0 && j<=2 && _running g && _board g ! (i,j) == Nothing
    then playCell (i,j) g
    else g

playCell :: Ix2 -> Game -> Game
playCell ij g0 = Game b1 p1 r1 n1
    where
        p0 = _player g0
        b1 = _board g0 // [(ij, p0)]
        n1 = 1 + _nmoves g0
        win = checkWin ij p0 b1
        r1 = not win && n1 < 9
        p1 = case (win, r1, p0) of
                (True,  False, _) -> p0                         -- win
                (False, False, _) -> Nothing                    -- tie
                (False, True, Just PlayerX) -> Just PlayerO     -- next player
                (False, True, Just PlayerO) -> Just PlayerX     -- next player
                _ -> error "invalid status (playCell)"

checkWin :: Ix2 -> Maybe Player -> Board -> Bool
checkWin (i,j) p b
    =  b!(i,0) == p && b!(i,1) == p && b!(i,2) == p  -- row
    || b!(0,j) == p && b!(1,j) == p && b!(2,j) == p  -- column
    || b!(0,0) == p && b!(1,1) == p && b!(2,2) == p  -- diag 1
    || b!(0,2) == p && b!(1,1) == p && b!(2,0) == p  -- diag 2


