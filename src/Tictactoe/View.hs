module Tictactoe.View where

import Control.Monad (forM_)
import Data.Array

import Tictactoe.Game

showCell :: Maybe Player -> String
showCell Nothing = "."
showCell (Just PlayerX) = "X"
showCell (Just PlayerO) = "O"

showStatus :: Game -> String
showStatus g = case (_running g, _player g) of
    (False, Nothing) -> "tie"
    (False, Just PlayerX) -> "X wins"
    (False, Just PlayerO) -> "O wins"
    (True, Nothing) -> error "invalid status (showStatus)"
    (True, p) -> showCell p ++ " plays"

printGame :: Game -> IO ()
printGame g = do
    forM_ [0..2] $ \i -> do
        forM_ [0..2] $ \j -> do
            let c = _board g ! (i, j)
            putStr $ " " ++ showCell c
        putStrLn ""
    putStrLn $ "status: " ++ showStatus g

